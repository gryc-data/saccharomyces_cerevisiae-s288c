## *Saccharomyces cerevisae* S288c

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJNA43747](https://www.ebi.ac.uk/ena/browser/view/PRJNA43747)
* **Assembly accession**: [GCA_000146045.2](https://www.ebi.ac.uk/ena/browser/view/GCA_000146045.2)
* **Original submitter**: Saccharomyces Genome Database (SGD)

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: R64
* **Assembly length**: 12,071,326
* **#Chromosomes**: 16
* **Mitochondiral**: No
* **N50 (L50)**: 924,431 (6)

### Annotation overview

* **Original annotator**: Saccharomyces Genome Database (SGD)
* **CDS count**: 5989
* **Pseudogene count**: 24
* **tRNA count**: 275
* **rRNA count**: 12
* **Mobile element count**: 50
