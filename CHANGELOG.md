# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.0 (2021-05-17)

### Added

* The 16 annotated chromosomes of Saccharomyces cerevisiae S288c (source EBI, [GCA_000146045.2](https://www.ebi.ac.uk/ena/browser/view/GCA_000146045.2)).
